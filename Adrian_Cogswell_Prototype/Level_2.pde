class Level2 {


  void display() {
    {    
      imageMode(CENTER);
      image(flyHand, 900+10*(sin(frameCount*.3)), y-50, 100, 165);
      imageMode(CORNER);
      move+=speed; //move speed across plane

      if (move>350) { //if circle location is greater than 150 that move back
        move = move-10;
        speed = -1;
      }
      if (move < 300) { //if circle is less than 0 that move forward
        move = 300;
        speed = 1;
      }
      if (tonguepos2.x > 900+10*(sin(frameCount*.3))-40 && tonguepos2.x < 900+10*(sin(frameCount*.3))+40 && tonguepos2.y < y+30 && tonguepos2.y > y-30) { //create the hitbox for level two booper
        level2.update();
      }
    }
  }


  void update() {
    tongueSaveY=0;  //rest bubble for froggo
    tongueSaveX=0;
    move =300; //reset move variables for the next level
    speed = 5;//reset speed variables for the next level
    level = 3;//reset level variables for the next level
    y = 300; //reset y variables for the next level
    println("Level "+level);    //display the level
  }
}
