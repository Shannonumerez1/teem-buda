

class Tongue {

  void update() {
    //when the projectile hits the first portal it will emerge out the second with the same directional velocity
    if (tonguepos2.y<=portalpos.y+20 && tonguepos2.y>=portalpos.y-20 && tonguepos2.x>=portalpos.x-50 && tonguepos2.x<=portalpos.x+50) {
      tonguepos2.y= portal2pos.y;
      tonguepos2.x= portal2pos.x;
    }
    //when the projectile hits the third portal, it will come out of the fourth, tested with turning it upside down, turning right and turning left
    if (tonguepos2.y>=portal3pos.y-20 && tonguepos2.x>=portal3pos.x-50 && tonguepos2.x<=portal3pos.x+50 && tonguepos2.y<=portal3pos.y+20) {
      //invertVert();
      //invertTurnRight();
      //invertTurnLeft();
      tonguepos2.y= portal4pos.y;
      tonguepos2.x= portal4pos.x;
    }
  }


  void display() {
    noStroke();
    rectMode(CENTER);
    fill(234, 232, 184);
    rect(tonguepos1.x, tonguepos1.y, 50, 50);
    imageMode(CENTER);
    image(bubble, tonguepos2.x, tonguepos2.y, 50, 50);    //display the bubble as the projector
    imageMode(CORNER);
    //fill(225, 207, 211);
    //rect(tonguepos2.x, tonguepos2.y, 30, 30);
  }

  void keyPressed() {
    //press Q to restart the bubble position and shoot
    if (key=='q'||key=='Q') {
      tongueSaveY=0;
      tongueSaveX=0;
      //(100, 500
      tonguepos2.x=110;
      tonguepos2.y=460;
      //
      FOverlay = false;
    }

    //press space to shoot/release the bubble
    if (key==' ') {
      pleaseWork();
      FOverlay = true;   
    }
  }

  //save the distance between the projectile and the portal before setting the velocity so that the projectile won't decrease in velocity over time
  //use the X and Y distance between the projectile's starting location and the first portal's current location to set the projectile's directional velocity
  void pleaseWork() {
    if (tonguepos2.y==460&&tonguepos2.x==110) {
      tongueSaveX=projDistance.x;
      tongueSaveY=projDistance.y;
    }
    tonguepos2.y=tonguepos2.y+tongueSaveY;
    tonguepos2.x=tonguepos2.x+tongueSaveX;
  }

  void invertVert() {
    //invert both Y and X velocities for when it comes out a portal facing a different direction, will want visual cues to show which direction a portal is facing
    tongueSaveY=-tongueSaveY;
    tongueSaveX=-tongueSaveX;
  }
  //turning the velocity 90 degrees to the right
  void invertTurnRight() {
    //save the Y velocity seperately before changing the Y velocity so that it won't interfere
    tongueTempSave=tongueSaveY;
    tongueSaveY=-tongueSaveX;
    tongueSaveX=-tongueTempSave;
  }
  //turning the velocity 90 degrees to the left
  void invertTurnLeft() {
    //save the Y velocity seperately before changing the Y velocity so that it won't interfere
    tongueTempSave=tongueSaveY;
    tongueSaveY=tongueSaveX;
    tongueSaveX=tongueTempSave;
  }
}
