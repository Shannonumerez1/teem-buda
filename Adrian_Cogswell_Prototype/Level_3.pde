class Level3 {
  void update() {

    imageMode(CENTER);
    image(flyHand, 600+250*(sin(frameCount*.08)), 85, 100, 165);
    imageMode(CORNER);
    move+=speed; //moves at speed of 5

    if (move>420) { //if circle is greater than 420 than move back at a faster speed
      move = move-10;
      speed = -20;
    }
    if (move < 200) { //if object is less than 200 than move forward
      move =200;
      speed = 2; //set speed of 2
    }
    if (tonguepos2.x > 600+250*(sin(frameCount*.08))-40 && tonguepos2.x < 600+250*(sin(frameCount*.08))+40 && tonguepos2.y < 85+10 && tonguepos2.y > 85-10) {    //
      //rectMode(CENTER);
      //fill(0);
      //rect(tonguepos2.x, tonguepos2.y, 50, 50);
      tongueSaveY=0;     //reset tongue variables for the next level
      tongueSaveX=0;    //reset tongue variables for the next level
      y=100;            // reset y variables variables for the next level
      level = 4;       //level = 4 and bring to end screen
      println("Level "+level); //display level
    }
  }
}
