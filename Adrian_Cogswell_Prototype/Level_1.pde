class Level1 {
  //Our first level

  void display() {

    if (level==1) { //if th level = 1 then display level one
      imageMode(CENTER);
      image(flyHand, x+(sin(frameCount)), y-50, 100, 165);
      imageMode(CORNER);
      if (tonguepos2.x > x-70 && tonguepos2.x < x+70 && tonguepos2.y < y+10 && tonguepos2.y > y-10) {  //create the hitbox for level one booper

        tongueSaveY=0;   //restart the tongue position to the froggo
        tongueSaveX=0;
        level = 2;  //change the level to level 2 if the hit box is hit
        println("Level "+level);    //display the level
      }
    }
  }
}
