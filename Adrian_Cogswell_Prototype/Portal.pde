/* the portal won't stop until another button is released for the time being, 
 and it will be converted into either a more functioning version or converted
 into mouse based movement, like click and drag or click to have the portal
 move toward the mouse's current position.
 */

class Portal {
  //move the portal based one mouse movement



  void update() {

    //if (callPortal==true); {
    //  portalpos.y=portalpos.y+portalDist.y;
    //  portalpos.x=portalpos.x+portalDist.x;
    //}
    //if (portalpos.y==portalDist.y&&portalpos.x==portalDist.x) {
    //  callPortal=false;
    //}
    portalpos.y=mouseY;
    portalpos.x=mouseX;

    if (portalpos.x>=250) {
      portalpos.x=250;
    }
    if (portalpos.x<=0) {
      portalpos.x=0;
    }
  }
  //display the portal placeholders
  void display() {
    noStroke();
    fill(225, 207, 211);
    ellipse(portalpos.x, portalpos.y, 50, 25);

    //the other one
    ellipse (portal2pos.x, portal2pos.y, 50, 25);

    //other other one
    ellipse (portal3pos.x, portal3pos.y, 50, 25);

    //other other other one
    ellipse (portal4pos.x, portal4pos.y, 50, 25);
  }
}




//move the portal based one WASD movement
void keyPressed() {
  if (key == 'w' || key == 'W') {
    up=true;
  } else if (key == 's'|| key == 'S') {
    down=true;
  } else if (key== 'a'|| key == 'A') {
    left=true;
  } else if (key== 'd'|| key == 'D') {
    right=true;
  } else if ((key != 'w' || key != 'W')&&(key != 's' || key != 'S')&&(key != 'a' || key != 'A')&&(key != 'd' || key != 'D')) {
    up=false;
    down=false;
    left=false;
    right=false;
  }

  //artifacts of trying to make the portal stop when the key is no longer pressed, abandonned since it is not essential to the game
  //tried while loops so that the 
  /*
     while (key=='w') {
   up=true;
   down=false;
   left=false;
   right=false;
   }
   while (key=='s') {
   up=false;
   down=true;
   left=false;
   right=false;
   }
   while (key=='a') {
   up=false;
   down=false;
   left=true;
   right=false;
   }
   while (key=='d') {
   up=false;
   down=false;
   left=false;
   right=true;
   }
   */
}



//void keyReleased() {
//  up=false;
//  down=false;
//  left=false;
//  right=false;
//}
