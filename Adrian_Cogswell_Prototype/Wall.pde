class Wall {

  int x1, x2, y1, y2;
  Wall(int x11, int y11, int x22, int y22) {
    x1=x11;
    x2=x22;
    y1=y11;
    y2=y22;
  }

  void display() {
    fill (0);
    rectMode(CORNERS);
    rect(x1, y1, x2, y2);
  }

  void update() {
    if (tonguepos2.x > x1-15 && tonguepos2.x < x2+15 && tonguepos2.y < y1+15 && tonguepos2.y > y2-15&&tongueSaveY!=0&&tongueSaveX!=0) {
      tongueSaveY=0;
      tongueSaveX=0;
    }
  }
}
