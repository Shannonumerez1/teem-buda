/*
Name: FROGGO 
 By: Shanno Umerez, Adrian Cogswell & Alice Peng
 
 This game is a portal like opuzzle game that requires the player to angle the portal right to pop the bubble using the moving hand. 
 The bubble will change speed depending on how far the portal is, it will also carry the angle of the bubble through each portal.
 Figure out how to pop your bubbles, good luck.
 
 Help lure Froggo out by  helping him catch the snoot boopers
 MOVE the portal with the mouse
 LAUNCH Froggo's doggo by pressing the space bar
 RECHARGE by pressing 'Q'
 */




import ddf.minim.*;
Minim minim;
AudioPlayer player;

//establishing variables
PVector portalpos, tonguepos1, tonguepos2, projDistance, portal2pos, portal3pos, portal4pos, portalDist;
boolean up, down, left, right;
float portalSpeed = 3;
float tongueSaveX, tongueSaveY, tongueTempSave;
int x = 525;
int y = 135;
int move = 300; //x axis
int speed = 1;
int level = 1;

boolean callPortal=false;
boolean startClick= false;
boolean instructions = false;
boolean isitworking = false;
PImage img1;
boolean FOverlay = false;
PImage background, well, Froggo, Froggo_overlay, Froggo_underlay, Portal, Portal2, Portal3, Portal4, bubble, flyHand, endscreen;

//creating classes
Level1 level1 = new Level1();
Level2 level2 = new Level2();
Level3 level3 = new Level3();

Portal portal = new Portal();
Tongue tongue = new Tongue();

//Wall wall = new Wall(0, 50, 50, 0);


Wall wall = new Wall(255, 540, 269, 0);

StartScreen start = new StartScreen();




void setup() {
  minim = new Minim(this);
  // this loads mysong.wav from the data folder
  player = minim.loadFile("Jazz.mp3");
  player.play();

  //loading all images
  background = loadImage("Level_background_base.png");
  well = loadImage("Well_Frame.png");
  Froggo = loadImage("Froggo.png");
  Froggo_overlay = loadImage("Froggo_overlay.png");
  Froggo_underlay= loadImage ("Froggo_underlay.png");
  Portal  = loadImage("Portal.png");
  Portal2 = loadImage("portal-blue.png");
  Portal3 = loadImage("Portal.png");
  Portal4 = loadImage("portal-blue.png");
  bubble= loadImage ("bubble.png");
  flyHand= loadImage ("Fly_Hand.png");
  endscreen = loadImage ("endscreen.png");


  //setting size and framerate
  size (960, 540);
  frameRate(60);

  //these PVectors are only supposed to initially start at these locations
  tonguepos2 = new PVector(110, 460);
  portalpos = new PVector(150, 100);
  img1 = loadImage ("Froggo.png");
}


void draw() {

  start.display();     //our starting screen
  image(img1, 315, 20);

  if (instructions == true) {      //if you click the instructions box then change screen to the instructions
    start.instructions();
  }
  if (startClick==true) {//these objects are stationary
    noCursor();
    wall.display();
    wall.update();

    portal2pos = new PVector (width/1.3, height/1.3);
    portal3pos = new PVector (width/1.3, height/2);
    portal4pos = new PVector (width/2, height/2);
    //this needs to update all the time, divided by 30 so that it moves to the portal in .5 seconds every time.
    projDistance = new PVector ((portalpos.x-tonguepos2.x)/30, (portalpos.y-tonguepos2.y)/30);
    portalDist = new PVector ((mouseX-portalpos.x)/30, (mouseY-portalpos.y)/30);
    tonguepos1 = new PVector(110, 460);

    portal.update();
    tongue.update();
    tongue.keyPressed();
    image(background, 270, 0, 689.5, 540);
    image(well, 0, 0, 270, 540);
    portal.display();

    tongue.display();
    //target.display();
    image(Portal, portalpos.x-62, portalpos.y-50, 126, 76);
    image(Portal2, portal2pos.x-62, portal2pos.y-50, 126, 76);
    image(Portal3, portal3pos.x-62, portal3pos.y-50, 126, 76);
    image(Portal4, portal4pos.x-62, portal4pos.y-50, 126, 76);
    level1.display();




    if (FOverlay == true) {
      //if image overlay is true the display the overlay
      image(Froggo_underlay, 50+24, 400+3*sin(frameCount*.2), 75, 40);
      image(Froggo_overlay, 50, 400+3*sin(frameCount*.2), 123, 139);
    } else {

      image(Froggo, 50, 400+3*sin(frameCount*.2), 123, 139);
    }
    //if level = 2 than display the second level fly
    if (level == 2) {
      level2.display();
    }
    //if level = 3 then display level 3 fly
    if (level == 3) {
      level3.update();
    }
  }
  end();
}

void mouseClicked() {          //click switch for starting screen 
  if (mouseX>150 && mouseX <450 && mouseY>400 && mouseY<500) {
    startClick=true;
  }
  if (mouseX>550 && mouseX<950 && mouseY>400 && mouseY<500) {  // if the mouseClick is inbetween these perameters than display the instructions
    instructions = true;
    if (mouseX>width/2-225 && mouseX<width/2+225 && mouseY>400 && mouseY<500) {
      instructions =false;
    }
  }
}



void end() {    //end of game and end screen display
  if (level == 4) {    //only show if level score = 4
    image(endscreen, 0, 0, 960, 540);
  }
}
