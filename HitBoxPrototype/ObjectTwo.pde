class ObjectTwo {
  int move = 200;
  int speed = 5;
  boolean trigger = false;


  void object() {
    fill(255);
    ellipse(move, height/2, 10, 10);
  }

  void update() {
    move+=speed; //moves at speed of 5
    if (move>420) { //if circle is greater than 420 than move back at a faster speed
      move = move-10;
      speed = -20;
    }
    if (move < 200) { //if object is less than 200 than move forward
      move =200;
      speed = 2; //set speed of 2
    }
    if (shooter<move+10&& shooter >move-10 &&bulletY==height/2){
      speed =0; //stop moving
      trigger = true; //set boolean to true
    }
  }
  void hit(){ //only if boolean is true
    if(trigger == true){
      fill(0);
      ellipse(move,height/2,20,20); //put hit ellipse to cover original object
    }
  }
}
