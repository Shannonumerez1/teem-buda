int shooter = 200;
int bulletY = 600;
int bulletposition;
int flyspeed = 10;
color ack = (0);
boolean bulletFired;

ObjectOne one = new ObjectOne();
ObjectTwo two = new ObjectTwo();
ObjectThree three = new ObjectThree();  //calling to all other classes


void setup() {
  size(600, 600);
}

void draw() {
  background(255);  //set backgrpund to white
  shootbox();
  barriers();

  one.object();
  one.update();
  one.hit();

  two.object();
  two.update();
  two.hit();

  three.object();
  three.hit();
  bullet();

  fill(255);
  ellipse(bulletposition, bulletY, 5, 5); //this is my bullet that will shoot once up is pressed
}



void barriers() {  //my barriers between each moving object to represent the level AI
  rect(150, height/2, 2, 400);
  rect(450, height/2, 2, 400);
}

void shootbox() { //shooter box that moves back and forth with arrow keys
  rectMode(CENTER);
  fill(0);
  rect(shooter, 600, 20, 10);
}


void bullet() { //bullet function
  if (bulletFired==true) { //bullet will only be fired once the boolean is true
    bulletY = bulletY-15; //bullets Y location will decrease each frame to move up screen
  }
}

void keyPressed() {
  if (key == CODED) {
    if (keyCode == LEFT) { //if left key is pressed shooter moves left
      shooter = shooter-10;
    }
    if (keyCode == RIGHT) { //if right key is pressed shooter moves right
      shooter = shooter +10;
    }
    if (keyCode == UP) { //if up key is pressed shooter fires a bullet
      bulletposition = shooter; //position of bullet is same as position of shooter
      bulletFired=true; //change boolean to true
      if (bulletY<0) {
        bulletY=600; //reset bullet once less than 0
      }
    }
  }
}
