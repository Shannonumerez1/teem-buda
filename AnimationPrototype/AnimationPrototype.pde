//Animation Prototype by Alice Peng 
/* note: the sprites were free assets found online and the music was taken from youtube, all as placeholder. 
my research included: 
- figuring out how to display idle animation 
- action animation for a short period of time when a certain key is pressed
- constant background music 
- action sound 
*/
//importing sound libary 
import processing.sound.*;
//creating sound files, one for background music and one for action
SoundFile file;
SoundFile action;
//variable
int count = 0;
//creating new arrays to hold the sprites
PImage[] images = new PImage[5];
PImage[] images2 = new PImage[4];

void setup() {
  //assigning each sound file to an actual file located in the folder
  //"yee" sound when space is pressed
  action = new SoundFile(this, "action.mp3"); 
  //background music
  file = new SoundFile(this, "music.mp3");
  //looping the background music
  file.loop(); 

//declaring size of canvas
  size(300, 300);
  //15 frames per second
  frameRate (15);
  //importing the pictures into each space of the arrays and setting their sizes bigger
  images[0] = loadImage("1.png");
  images[1] = loadImage("2.png");
  images[2] = loadImage("3.png");
  images[3] = loadImage("4.png");
  images[4] = loadImage("0.png");
  images[0].resize(80, 80); //image size were originally 50 x 50
  images[1].resize(80, 80);
  images[2].resize(80, 80);
  images[3].resize(80, 80);
  images[4].resize(80, 80);


  images2[0] = loadImage("11.png");
  images2[1] = loadImage("22.png");
  images2[2] = loadImage("33.png");
  images2[3] = loadImage("44.png");
  images2[0].resize(80, 80);
  images2[1].resize(80, 80);
  images2[2].resize(80, 80);
  images2[3].resize(80, 80);
}

//the draw function
void draw() {
  //black background
  background(0);
  //calls on the idle function
  idle();
}

//idle function shows the idle animation when no action is takken, and the action animtion when space is pressed.
void idle() {
  if (keyPressed == true) {
    if (key == 32) { 
      count = frameCount+5;
    }
  }
  if (frameCount-count<15 && frameCount>20) { //uses framecount to display the action animation for 20 frames, which is around 1 second. 
    image(images2[(frameCount) % 4], 110, 110);
  } else {
    image(images[(frameCount) % 5], 110, 110);
  }
}


//function for if a key is pressed
void keyPressed() {
  if (key == 32) { //if the key is space
  //plays the "yee" sound file 
    action.play();
  }
}
